# sysInfo

## 简介

sysInfo使用 Python Django 框架和 psutil 开发的一个中文版 Linux 服务器信息查看应用，可查看的信息包括系统、CPU、内存、硬盘、进程、网络、登录用户等，同时可查看并导出部分数据的图表。

## 需要安装的python包


## 运行

- 下载或使用 `git clone` 获取源代码
- 进入目录执行 `python manage.py runserver 9999` 启动应用
- web 页面访问 http://localhost:9999 ，修改 auto-refresh.js 文件中的 notRefresh 可配置是否自动刷新。

## 运行截图

### 首页

![img.png](static/Screenshot/img.png)

### 用户页

![img_1.png](static/Screenshot/img_1.png)

### cpu表格显示

![img.png](static/Screenshot/img_4.png)

### cpu饼图显示

![img.png](static/Screenshot/img_3.png)

### cpu折线显示

![img.png](static/Screenshot/img_2.png)